@echo off
rem -------------------------------------------------------------
rem  사용설명 
rem  본파일을 설치하려는 폴더 상위에 놓고 실행을 하면 됨.
rem -------------------------------------------------------------

pushd "%~dp0"
echo .
echo .
echo =====================================
echo    Python 다운로드  
echo =====================================
:PYTHON_Q
set /p PYN=Python을 다운 받겠습니까? (Y/N)?
if /i "%PYN%" == "y" goto PYTHON_DOWN
if /i "%PYN%" == "n" goto PYTHON_MODULE
goto PYTHON_Q

:PYTHON_DOWN
call c:\windows\explorer.exe https://www.python.org/ftp/python/2.7.14/python-2.7.14.amd64.msi
echo .
echo .
echo   다운받은 Python을 C:\Python27 에 설치하고 나서 아무키나 누르세요
echo .
pause

:PYTHON_MODULE
echo .
echo .
echo -------------------------------------
echo    Python 모듈설치 ( pyperclip, hanziconv)
echo -------------------------------------
:PYTHON_MODULE_Q
set /p PMYN=Python 모듈(pyperclip, hanziconv)을 다운 받겠습니까? (Y/N)?
if /i "%PMYN%" == "y" goto PYTHON_MODULE_DOWN
if /i "%PMYN%" == "n" goto GIT
goto PYTHON_MODULE_Q

:PYTHON_MODULE_DOWN
call C:\Python27\Scripts\pip install pyperclip==1.6.1
call C:\Python27\Scripts\pip install hanziconv

:GIT
echo .
echo .
echo =====================================
echo    Git 다운로드  
echo =====================================
:GIT_Q
set /p GYN=Git을 다운 받겠습니까? (Y/N)?
if /i "%GYN%" == "y" goto GIT_DOWN
if /i "%GYN%" == "n" goto GIT_SRC
goto GIT_Q

:GIT_DOWN
call  c:\windows\explorer.exe https://git-scm.com/download/win
echo .
echo .
echo   다운받은 Git을 설치하고 나서 아무키나 누르세요
echo .
pause

:GIT_SRC
echo .
echo .
echo -------------------------------------
echo    pyscemi 소스 다운받기
echo -------------------------------------
:GIT_SRC_Q
set /p GSYN=pyscemi 소스를 다운 받겠습니까? (Y/N)?
if /i "%GSYN%" == "y" goto GIT_SRC_DOWN
if /i "%GSYN%" == "n" goto END
goto GIT_SRC_Q

:GIT_SRC_DOWN
git clone https://bitbucket.org/ianguylee/pyscemi.git

:END
pause