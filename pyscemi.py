#-*- coding: utf-8 -*-

#from __future__ import print_function

import pyperclip
import time
import msvcrt

from _scesc import SpecialCharEsc


prev_clipboard = ''	

def main():
    global prev_clipboard
    
    strn = pyperclip.paste()
    # 클립보드가 비어 있거나 바뀐게 없으면 pass
    if len(strn) <= 0 or prev_clipboard == strn:
        return

    #print '\n=== IN - START ==='
    #print strn
    #print '=== IN - END ==='
	
	# uci:는 변환을 안한다.
    if strn.lower().find("uci:") >=0 :
        prev_clipboard = strn
        return

    strn = SpecialCharEsc(strn)
    
    pyperclip.copy(strn)
    print ('\n=== OUT - START ===')
    try:
        print (strn)
    except UnicodeEncodeError as e:
        print( e)
        f = open(u'encode_error.txt', 'a')
        f.write(str(e)+'\r\n')
        f.close()
    print ('=== OUT - END ===')

    prev_clipboard = strn

import sys
if __name__=="__main__":
    cnt = 0
    do_job = 1
    key = ''
    while 1:
        try:
            if do_job :
                main()
                cnt += 1
                print ( u'.' if cnt%2 else u'-')
                if cnt%20 == 0 : print( u'q or Q for exit' )
            if msvcrt.kbhit():
                key = msvcrt.getch()
                print(key)
                if key in ( b'q',b'Q' ) :
                    print ('exit')
                    break
                do_job = 1 - do_job
                print ( u'Start' if do_job else u'Pause' )
                
            time.sleep(1)
        except:
            1  # windows에서 콘손 화면 표시로 잡았다가 취소를 하면 IOError발생
            print (sys.exc_info()[0])
            print (sys.exc_info()[1])
            time.sleep(1)
        
        
