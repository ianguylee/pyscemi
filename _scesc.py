#-*- coding: utf-8 -*-

import sys
import unicodedata
from hanziconv import HanziConv
#-------------------------------------------------------------
# 변환 문자 추가 시 아래에 넣는다.
#-------------------------------------------------------------
# translate를 이용해서 한글자 => 한글자로 변환
_trans_spc_src = { 
	u'oldChars' : u'~!@#$%&*<>^+=:/;?\\Øßё"\'–©«»—₩•･▪✘✗✔✓∙｢｣' ,
	u'newChars' : u'∼！＠＃＄％＆＊＜＞＾＋＝：／；？￦OBe＂`-ⓒ≪≫-￦···Xx√√·「」'
}

# replace를 이용해서 n글자 => m글자로 변환
_replace_src = {
	u'æ' : u'ae',
        u'\u200b' : u' ',   # Zero-width space
		u'\u200e' : u'',    # LEFT-to-RIGHT mark
        u'\u2028' : u'\u000A', #LINE SEPARATOR
        u'\u2029' : u'\u000A', #PARAGRAPH SEPARATOR
		u'\u202C' : u'',    # POP DIRECTIONAL FORMATTING			
        u'\ud83c' : u'？',
        u'\udf0e' : u'？',
        u'\ufffd' : u'？',        
        u'\xa0'   : u' ',   # NBSP non-breaking space
        u'\u30fc' : u'-',    # KATAKANA-HIRAGANA PROLONGED SOUND MARK
        u'\u30fb' : u'·',    # KATAKANA MIDDLE DOT
		u'\u0085' : u'',    # NEXT LINE (NEL) … 
        
}

#private use area ( U+E000 – U+F8FF ) : unicode http://www.alanwood.net/unicode/private_use_area.html

_trans_jpkanji_src = {
        u'oldChars' :(u'乗乱亜仏来併仮伝偽価倹児内両冰剰剣剤労勲励勧区巻即参呉単厳嘱圏国囲図団増堕圧塁壊壌壮壱寿奥奨尚姫娯嬢学寝実写寛宝将専対届属峡岳巌岩巣帯并廃広庁弾弥径従徴徳恆'
                      u'恵悪悩悦慎惨応懐恋戦戯戻払抜拝挟挿掲揺捜択撃担拠挙拡摂収効叙敕教数断既晚昼暦暁曽会条桟栄概楽楼枢様横検桜権欧歓歩歴歳帰残殻殴毎気渉涙浄清浅渇渓温滞満潜渋沢湿済'
                      u'滝瀬湾灯焼営炉争為犠状狭独猟獣献瓶画当畳痩発盗尽真研砕祕禅礼税称稲穂穏窃並粋糸経緑緒縁県縦総縄絵継続繊欠缶声聡聴粛脳脱胆臓台与旧艷荘茎莱万薫蔵芸薬処虚号蛍虫蚕'
                      u'蛮衞装襃覚覧観触説謡証訳誉読変譲豐予弐売頼賛践軽転弁辞遥逓遅辺郎郷酔医醸釈鋭録銭錬鎮鉄鋳鉱閲関陥随険隠双雑霸霊青静顕翻飲余餅騒駆験駅髄体髪闘鶏塩麦麺黄黒黙点党斉斎歯齢竜亀' ) ,
        
        u'newChars' :(u'乘亂亞佛來倂假傳僞價儉兒內兩氷剩劍劑勞勳勵勸區卷卽參吳單嚴囑圈國圍圖團增墮壓壘壞壤壯壹壽奧奬尙姬娛孃學寢實寫寬寶將專對屆屬峽嶽巖巖巢帶幷廢廣廳彈彌徑從徵德恒'
                      u'惠惡惱悅愼慘應懷戀戰戲戾拂拔拜挾插揭搖搜擇擊擔據擧擴攝收效敍勅敎數斷旣晩晝曆曉曾會條棧榮槪樂樓樞樣橫檢櫻權歐歡步歷歲歸殘殼毆每氣涉淚淨淸淺渴溪溫滯滿潛澁澤濕濟'
                      u'瀧瀨灣燈燒營爐爭爲犧狀狹獨獵獸獻甁畵當疊瘦發盜盡眞硏碎秘禪禮稅稱稻穗穩竊竝粹絲經綠緖緣縣縱總繩繪繼續纖缺罐聲聰聽肅腦脫膽臟臺與舊艶莊莖萊萬薰藏藝藥處虛號螢蟲蠶'
                      u'蠻衛裝褒覺覽觀觸說謠證譯譽讀變讓豊豫貳賣賴贊踐輕轉辨辭遙遞遲邊郞鄕醉醫釀釋銳錄錢鍊鎭鐵鑄鑛閱關陷隨險隱雙雜覇靈靑靜顯飜飮餘餠騷驅驗驛髓體髮鬪鷄鹽麥麵黃黑默點黨齊齋齒齡龍龜' )
}

_trans_trad_han_src = {
        u'oldChars' :(u'鞦榖睏昂戲鬍喰咲雫鼕' ) ,        
        u'newChars' :(u'秋谷困昻戱胡飡笑雩冬' )
}

if (sys.version_info > (3,0)):
	unichr = chr

_latin_combing =  ( unichr(ch) for ch in range( ord(u'\u0300'), ord(u'\u036F') )) # Unicode Block 'Combining Diacritical Marks'

_latin_combing = tuple(_latin_combing) + ( u'\u0483', u'\u0484', u'\u0485', u'\u0486', u'\u0487', 
u'\u0592', u'\u0593', u'\u0594', u'\u0595', u'\u0597', u'\u0598', u'\u0599', u'\u059C', u'\u059D', u'\u059E', u'\u059F', u'\u05A0', u'\u05A1', u'\u05A8', 
u'\u05A9', u'\u05AB', u'\u05AC', u'\u05AF', u'\u05C4', u'\u0610', u'\u0611', u'\u0612', u'\u0613', u'\u0614', u'\u0615', u'\u0616', u'\u0617', u'\u0653', 
u'\u0654', u'\u0657', u'\u0658', u'\u0659', u'\u065A', u'\u065B', u'\u065D', u'\u065E', u'\u06D6', u'\u06D7', u'\u06D8', u'\u06D9', u'\u06DA', u'\u06DB', 
u'\u06DC', u'\u06DF', u'\u06E0', u'\u06E1', u'\u06E2', u'\u06E4', u'\u06E7', u'\u06E8', u'\u06EB', u'\u06EC', u'\u0730', u'\u0732', u'\u0733', u'\u0735', 
u'\u0736', u'\u073A', u'\u073D', u'\u073F', u'\u0740', u'\u0741', u'\u0743', u'\u0745', u'\u0747', u'\u0749', u'\u074A', u'\u07EB', u'\u07EC', u'\u07ED', 
u'\u07EE', u'\u07EF', u'\u07F0', u'\u07F1', u'\u07F3', u'\u0816', u'\u0817', u'\u0818', u'\u0819', u'\u081B', u'\u081C', u'\u081D', u'\u081E', u'\u081F', 
u'\u0820', u'\u0821', u'\u0822', u'\u0823', u'\u0825', u'\u0826', u'\u0827', u'\u0829', u'\u082A', u'\u082B', u'\u082C', u'\u082D', u'\u08D4', u'\u08D5', 
u'\u08D6', u'\u08D7', u'\u08D8', u'\u08D9', u'\u08DA', u'\u08DB', u'\u08DC', u'\u08DD', u'\u08DE', u'\u08DF', u'\u08E0', u'\u08E1', u'\u08E4', u'\u08E5', 
u'\u08E7', u'\u08E8', u'\u08EA', u'\u08EB', u'\u08EC', u'\u08F3', u'\u08F4', u'\u08F5', u'\u08F7', u'\u08F8', u'\u08FB', u'\u08FC', u'\u08FD', u'\u08FE', 
u'\u08FF', u'\u0951', u'\u0953', u'\u0954', u'\u0F82', u'\u0F83', u'\u0F86', u'\u0F87', u'\u135D', u'\u135E', u'\u135F', u'\u17DD', u'\u193A', u'\u1A17', 
u'\u1A75', u'\u1A76', u'\u1A77', u'\u1A78', u'\u1A79', u'\u1A7A', u'\u1A7B', u'\u1A7C', u'\u1AB0', u'\u1AB1', u'\u1AB2', u'\u1AB3', u'\u1AB4', u'\u1ABB', 
u'\u1ABC', u'\u1B6B', u'\u1B6D', u'\u1B6E', u'\u1B6F', u'\u1B70', u'\u1B71', u'\u1B72', u'\u1B73', u'\u1CD0', u'\u1CD1', u'\u1CD2', u'\u1CDA', u'\u1CDB', 
u'\u1CE0', u'\u1CF4', u'\u1CF8', u'\u1CF9', u'\u1DC0', u'\u1DC1', u'\u1DC3', u'\u1DC4', u'\u1DC5', u'\u1DC6', u'\u1DC7', u'\u1DC8', u'\u1DC9', u'\u1DCB', 
u'\u1DCC', u'\u1DD1', u'\u1DD2', u'\u1DD3', u'\u1DD4', u'\u1DD5', u'\u1DD6', u'\u1DD7', u'\u1DD8', u'\u1DD9', u'\u1DDA', u'\u1DDB', u'\u1DDC', u'\u1DDD', 
u'\u1DDE', u'\u1DDF', u'\u1DE0', u'\u1DE1', u'\u1DE2', u'\u1DE3', u'\u1DE4', u'\u1DE5', u'\u1DE6', u'\u1DE7', u'\u1DE8', u'\u1DE9', u'\u1DEA', u'\u1DEB', 
u'\u1DEC', u'\u1DED', u'\u1DEE', u'\u1DEF', u'\u1DF0', u'\u1DF1', u'\u1DF2', u'\u1DF3', u'\u1DF4', u'\u1DF5', u'\u1DFB', u'\u1DFE', u'\u20D0', u'\u20D1', 
u'\u20D4', u'\u20D5', u'\u20D6', u'\u20D7', u'\u20DB', u'\u20DC', u'\u20E1', u'\u20E7', u'\u20E9', u'\u20F0', u'\u2CEF', u'\u2CF0', u'\u2CF1', u'\u2DE0', 
u'\u2DE1', u'\u2DE2', u'\u2DE3', u'\u2DE4', u'\u2DE5', u'\u2DE6', u'\u2DE7', u'\u2DE8', u'\u2DE9', u'\u2DEA', u'\u2DEB', u'\u2DEC', u'\u2DED', u'\u2DEE', 
u'\u2DEF', u'\u2DF0', u'\u2DF1', u'\u2DF2', u'\u2DF3', u'\u2DF4', u'\u2DF5', u'\u2DF6', u'\u2DF7', u'\u2DF8', u'\u2DF9', u'\u2DFA', u'\u2DFB', u'\u2DFC', 
u'\u2DFD', u'\u2DFE', u'\u2DFF', u'\uA66F', u'\uA674', u'\uA675', u'\uA676', u'\uA677', u'\uA678', u'\uA679', u'\uA67A', u'\uA67B', u'\uA67C', u'\uA67D', 
u'\uA69E', u'\uA69F', u'\uA6F0', u'\uA6F1', u'\uA8E0', u'\uA8E1', u'\uA8E2', u'\uA8E3', u'\uA8E4', u'\uA8E5', u'\uA8E6', u'\uA8E7', u'\uA8E8', u'\uA8E9', 
u'\uA8EA', u'\uA8EB', u'\uA8EC', u'\uA8ED', u'\uA8EE', u'\uA8EF', u'\uA8F0', u'\uA8F1', u'\uAAB0', u'\uAAB2', u'\uAAB3', u'\uAAB7', u'\uAAB8', u'\uAABE', 
u'\uAABF', u'\uAAC1', u'\uFE20', u'\uFE21', u'\uFE22', u'\uFE23', u'\uFE24', u'\uFE25', u'\uFE26', u'\uFE2E', u'\uFE2F', u'\u031B', u'\u0323' )

#-------------------------------------------------------------

def getFullwidthString( str ):
	global _trans_spc_src, _replace_src
	
	#한글자 2 한글자 변환
	tbl = dict(zip(map(ord,_trans_spc_src['oldChars']), map(ord,_trans_spc_src['newChars'])))
	str = str.translate(tbl)
	
	#여러글자 2 여러글자 변환
	for key, val in _replace_src.items():
		str = str.replace(key, val)
		
	return str

def getKrHanjafromJpKanji( str ):
	global _trans_jpkanji_src
	
	#일본 신자체 한자 2 구자체 한자
	tbl = dict(zip(map(ord,_trans_jpkanji_src['oldChars']), map(ord,_trans_jpkanji_src['newChars'])))
	str = str.translate(tbl)
	
	return str

def getKrHanjafromTraditionalChinese( str ):
	global _trans_trad_han_src
	
	#본자 to 한국 한자
	tbl = dict(zip(map(ord,_trans_trad_han_src['oldChars']), map(ord,_trans_trad_han_src['newChars'])))
	str = str.translate(tbl)
	
	return str

def getNormalLatinAlphabet( str):
	#accents = ('COMBINING ACUTE ACCENT', 'COMBINING GRAVE ACCENT', 'COMBINING TILDE', 'COMBINING MACRON', 'COMBINING CARON')
	#accents = set(map(unicodedata.lookup, accents))
	accents = _latin_combing
	chars = [c for c in unicodedata.normalize('NFD', str) if c not in accents]
	return unicodedata.normalize('NFC', ''.join(chars))        

def getAcceptableStr( istr): # cp949에 없는 글자만 convert
	outstr = ""
	for c in istr:
		d = c
		if b'?' != c :
			d = c.encode('euc-kr','replace')
			if b'' == d or b'?' == d:
				d = HanziConv.toTraditional(c)
				d = getNormalLatinAlphabet(d)
				d = getKrHanjafromTraditionalChinese(d)				
			else :
				d = c
		outstr += d		
	return outstr

#---------------------------------------
def SpecialCharEsc( tstr):
	tstr = getFullwidthString( tstr)	
	tstr = getKrHanjafromJpKanji(tstr)
	tstr = getAcceptableStr( tstr)
	return tstr


if __name__=="__main__":
	m = u'\"우리æ나亜仏来亞佛來라齢竜亀齡龍龜@#$\'François-René Duchâble'
	ret1 = getFullwidthString( m)
	ret2 = getKrHanjafromJpKanji(ret1)
	ret3 = SpecialCharEsc(m)
	print(m)
	print(ret1)
	print(ret2)
	print(ret3)
	
